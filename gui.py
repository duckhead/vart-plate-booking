# -*- coding: utf-8 -*- 

###########################################################################
## Python code generated with wxFormBuilder (version Feb 26 2014)
## http://www.wxformbuilder.org/
##
## PLEASE DO "NOT" EDIT THIS FILE!
###########################################################################

import wx
import wx.xrc

###########################################################################
## Class MyFrame1
###########################################################################

class MyFrame1 ( wx.Frame ):

    def __init__( self, parent ):
        wx.Frame.__init__ ( self, parent, id = wx.ID_ANY, title = wx.EmptyString, pos = wx.DefaultPosition, size = wx.Size( 701,758 ), style = wx.DEFAULT_FRAME_STYLE|wx.TAB_TRAVERSAL )

        self.SetSizeHintsSz( wx.DefaultSize, wx.DefaultSize )

        bSizer5 = wx.BoxSizer( wx.VERTICAL )

        gSizer6 = wx.GridSizer( 0, 2, 0, 0 )

        bSizer6 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText9 = wx.StaticText( self, wx.ID_ANY, u"Plate 1", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9.Wrap( -1 )
        self.m_staticText9.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )

        bSizer6.Add( self.m_staticText9, 0, wx.ALL, 5 )

        m_choice3Choices = [ u"Marcus", u"David", u"Peter", u"Simon", u"Sander", u"Marcus" ]
        self.m_choice3 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice3Choices, 0 )
        self.m_choice3.SetSelection( 0 )
        bSizer6.Add( self.m_choice3, 0, wx.ALL, 5 )

        self.m_button1 = wx.Button( self, wx.ID_ANY, u"Book me!", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6.Add( self.m_button1, 0, wx.ALL, 5 )


        gSizer6.Add( bSizer6, 1, wx.EXPAND, 5 )

        bSizer7 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText13 = wx.StaticText( self, wx.ID_ANY, u"Note:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText13.Wrap( -1 )
        bSizer7.Add( self.m_staticText13, 0, wx.ALL, 5 )

        self.m_textCtrl1 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer7.Add( self.m_textCtrl1, 1, wx.ALL|wx.EXPAND, 5 )


        gSizer6.Add( bSizer7, 1, wx.EXPAND, 5 )


        bSizer5.Add( gSizer6, 1, wx.EXPAND, 5 )

        self.m_staticline2 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer5.Add( self.m_staticline2, 0, wx.EXPAND |wx.ALL, 5 )

        gSizer61 = wx.GridSizer( 0, 2, 0, 0 )

        bSizer61 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText91 = wx.StaticText( self, wx.ID_ANY, u"Plate 2", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText91.Wrap( -1 )
        self.m_staticText91.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )

        bSizer61.Add( self.m_staticText91, 0, wx.ALL, 5 )

        m_choice31Choices = [ u"Marcus", u"David", u"Peter", u"Simon", u"Sander", u"Marcus" ]
        self.m_choice31 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice31Choices, 0 )
        self.m_choice31.SetSelection( 0 )
        bSizer61.Add( self.m_choice31, 0, wx.ALL, 5 )

        self.m_button2 = wx.Button( self, wx.ID_ANY, u"Book me!", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer61.Add( self.m_button2, 0, wx.ALL, 5 )


        gSizer61.Add( bSizer61, 1, wx.EXPAND, 5 )

        bSizer71 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText131 = wx.StaticText( self, wx.ID_ANY, u"Note:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText131.Wrap( -1 )
        bSizer71.Add( self.m_staticText131, 0, wx.ALL, 5 )

        self.m_textCtrl2 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer71.Add( self.m_textCtrl2, 1, wx.ALL|wx.EXPAND, 5 )


        gSizer61.Add( bSizer71, 1, wx.EXPAND, 5 )


        bSizer5.Add( gSizer61, 1, wx.EXPAND, 5 )

        self.m_staticline3 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer5.Add( self.m_staticline3, 0, wx.EXPAND |wx.ALL, 5 )

        gSizer62 = wx.GridSizer( 0, 2, 0, 0 )

        bSizer62 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText92 = wx.StaticText( self, wx.ID_ANY, u"Plate 3", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText92.Wrap( -1 )
        self.m_staticText92.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )

        bSizer62.Add( self.m_staticText92, 0, wx.ALL, 5 )

        m_choice32Choices = [ u"Marcus", u"David", u"Peter", u"Simon", u"Sander", u"Marcus" ]
        self.m_choice32 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice32Choices, 0 )
        self.m_choice32.SetSelection( 0 )
        bSizer62.Add( self.m_choice32, 0, wx.ALL, 5 )

        self.m_button3 = wx.Button( self, wx.ID_ANY, u"Book me!", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer62.Add( self.m_button3, 0, wx.ALL, 5 )


        gSizer62.Add( bSizer62, 1, wx.EXPAND, 5 )

        bSizer72 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText132 = wx.StaticText( self, wx.ID_ANY, u"Note:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText132.Wrap( -1 )
        bSizer72.Add( self.m_staticText132, 0, wx.ALL, 5 )

        self.m_textCtrl3 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer72.Add( self.m_textCtrl3, 1, wx.ALL|wx.EXPAND, 5 )


        gSizer62.Add( bSizer72, 1, wx.EXPAND, 5 )


        bSizer5.Add( gSizer62, 1, wx.EXPAND, 5 )

        self.m_staticline4 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer5.Add( self.m_staticline4, 0, wx.EXPAND |wx.ALL, 5 )

        gSizer621 = wx.GridSizer( 0, 2, 0, 0 )

        bSizer621 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText921 = wx.StaticText( self, wx.ID_ANY, u"Plate 5", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText921.Wrap( -1 )
        self.m_staticText921.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )

        bSizer621.Add( self.m_staticText921, 0, wx.ALL, 5 )

        m_choice321Choices = [ u"Marcus", u"David", u"Peter", u"Simon", u"Sander", u"Marcus" ]
        self.m_choice321 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice321Choices, 0 )
        self.m_choice321.SetSelection( 0 )
        bSizer621.Add( self.m_choice321, 0, wx.ALL, 5 )

        self.m_button4 = wx.Button( self, wx.ID_ANY, u"Book me!", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer621.Add( self.m_button4, 0, wx.ALL, 5 )


        gSizer621.Add( bSizer621, 1, wx.EXPAND, 5 )

        bSizer721 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText1321 = wx.StaticText( self, wx.ID_ANY, u"Note:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText1321.Wrap( -1 )
        bSizer721.Add( self.m_staticText1321, 0, wx.ALL, 5 )

        self.m_textCtrl4 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer721.Add( self.m_textCtrl4, 1, wx.ALL|wx.EXPAND, 5 )


        gSizer621.Add( bSizer721, 1, wx.EXPAND, 5 )


        bSizer5.Add( gSizer621, 1, wx.EXPAND, 5 )

        self.m_staticline5 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer5.Add( self.m_staticline5, 0, wx.EXPAND |wx.ALL, 5 )

        gSizer6211 = wx.GridSizer( 0, 2, 0, 0 )

        bSizer6211 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText9211 = wx.StaticText( self, wx.ID_ANY, u"Plate 6 (TDA)", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText9211.Wrap( -1 )
        self.m_staticText9211.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )

        bSizer6211.Add( self.m_staticText9211, 0, wx.ALL, 5 )

        m_choice3211Choices = [ u"Marcus", u"David", u"Peter", u"Simon", u"Sander", u"Marcus" ]
        self.m_choice3211 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice3211Choices, 0 )
        self.m_choice3211.SetSelection( 0 )
        bSizer6211.Add( self.m_choice3211, 0, wx.ALL, 5 )

        self.m_button5 = wx.Button( self, wx.ID_ANY, u"Book me!", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer6211.Add( self.m_button5, 0, wx.ALL, 5 )


        gSizer6211.Add( bSizer6211, 1, wx.EXPAND, 5 )

        bSizer7211 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText13211 = wx.StaticText( self, wx.ID_ANY, u"Note:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText13211.Wrap( -1 )
        bSizer7211.Add( self.m_staticText13211, 0, wx.ALL, 5 )

        self.m_textCtrl5 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer7211.Add( self.m_textCtrl5, 1, wx.ALL|wx.EXPAND, 5 )


        gSizer6211.Add( bSizer7211, 1, wx.EXPAND, 5 )


        bSizer5.Add( gSizer6211, 1, wx.EXPAND, 5 )

        self.m_staticline6 = wx.StaticLine( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, wx.LI_HORIZONTAL )
        bSizer5.Add( self.m_staticline6, 0, wx.EXPAND |wx.ALL, 5 )

        gSizer62111 = wx.GridSizer( 0, 2, 0, 0 )

        bSizer62111 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText92111 = wx.StaticText( self, wx.ID_ANY, u"Plate 7 (5xCVC)", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText92111.Wrap( -1 )
        self.m_staticText92111.SetFont( wx.Font( 20, 70, 90, 90, False, wx.EmptyString ) )

        bSizer62111.Add( self.m_staticText92111, 0, wx.ALL, 5 )

        m_choice32111Choices = [ u"Marcus", u"David", u"Peter", u"Simon", u"Sander", u"Marcus" ]
        self.m_choice32111 = wx.Choice( self, wx.ID_ANY, wx.DefaultPosition, wx.DefaultSize, m_choice32111Choices, 0 )
        self.m_choice32111.SetSelection( 0 )
        bSizer62111.Add( self.m_choice32111, 0, wx.ALL, 5 )

        self.m_button6 = wx.Button( self, wx.ID_ANY, u"Book me!", wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer62111.Add( self.m_button6, 0, wx.ALL, 5 )


        gSizer62111.Add( bSizer62111, 1, wx.EXPAND, 5 )

        bSizer72111 = wx.BoxSizer( wx.VERTICAL )

        self.m_staticText132111 = wx.StaticText( self, wx.ID_ANY, u"Note:", wx.DefaultPosition, wx.DefaultSize, 0 )
        self.m_staticText132111.Wrap( -1 )
        bSizer72111.Add( self.m_staticText132111, 0, wx.ALL, 5 )

        self.m_textCtrl6 = wx.TextCtrl( self, wx.ID_ANY, wx.EmptyString, wx.DefaultPosition, wx.DefaultSize, 0 )
        bSizer72111.Add( self.m_textCtrl6, 1, wx.ALL|wx.EXPAND, 5 )


        gSizer62111.Add( bSizer72111, 1, wx.EXPAND, 5 )


        bSizer5.Add( gSizer62111, 1, wx.EXPAND, 5 )


        self.SetSizer( bSizer5 )
        self.Layout()

        self.Centre( wx.BOTH )

        # Connect Events
        self.m_button1.Bind( wx.EVT_BUTTON, self.book_plate )

    def __del__( self ):
        pass





