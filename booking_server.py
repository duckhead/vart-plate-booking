#!/usr/bin/env python
from SimpleXMLRPCServer import SimpleXMLRPCServer
from ConfigParser import SafeConfigParser


class server:
    def __init__(self):
        # self.file = open("bookings.ini", "w")
        self.config = SafeConfigParser()
        self.config.add_section('plate_1')
        self.config.add_section('plate_2')
        self.config.add_section('plate_3')
        self.config.add_section('plate_4')
        self.config.add_section('plate_5')
        self.config.add_section('plate_6')

    def get_plate_status(self, plate):
        try:
            self.config.read('bookings.ini')
        except Exception:
            return False
        try:
            status = self.config.get(plate, 'status')
        except:
            return "FREE"
        return status

    def free_plate(self, plate):
        self.config.remove_option(plate, 'name')
        self.config.remove_option(plate, 'notes')
        self.config.remove_option(plate, 'status')

        try:
            with open('bookings.ini', 'w') as f:
                self.config.write(f)
        except Exception:
            return False
        return True

    # Expose a function
    def book_plate(self, plate, name, notes):
        self.config.set(plate, 'name', name)
        self.config.set(plate, 'notes', notes)
        self.config.set(plate, 'status', "BOOKED")

        try:
            with open('bookings.ini', 'w') as f:
                self.config.write(f)
            f.close()
        except Exception:
            return False
        return True

    def run_server(self):
        server = SimpleXMLRPCServer(('localhost', 9000), logRequests=True)
        server.register_function(self.book_plate)
        server.register_function(self.free_plate)
        server.register_function(self.get_plate_status)

        try:
            print 'Use Control-C to exit'
            server.serve_forever()
        except KeyboardInterrupt:
            print 'Exiting'

if __name__ == '__main__':
    vart_server = server()
    vart_server.run_server()



