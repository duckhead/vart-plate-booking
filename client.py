import xmlrpclib
import wx
import gui

# inherit from the MainFrame created in wxFowmBuilder and create CalcFrame
class VARTFrame(gui.MyFrame1):
    # constructor
    def __init__(self, parent):
        self.proxy = xmlrpclib.ServerProxy('http://localhost:9000')
        # initialize parent class
        gui.MyFrame1.__init__(self, parent)

    # Virtual event handler
    def book_plate(self, event):
        self.proxy.book_plate("plate_1", str(self.m_choice3.GetSelection()), self.m_textCtrl1.GetValue())

    def free_plate(self, plate):
        self.proxy.free_plate(plate)

    def get_plate_status(self, plate):
        return self.proxy.get_plate_status(plate)

# mandatory in wx, create an app, False stands for not deteriction stdin/stdout
# refer manual for details
app = wx.App(False)

# create an object of CalcFrame
frame = VARTFrame(None)
# show the frame
frame.Show(True)
# start the applications
app.MainLoop()
